﻿using UnityEngine;
using System.Collections;

public class Communication : MonoBehaviour {

	public Script1 publicZmienna;
	private Script2 przezKomponent;

	void Start()
	{
		przezKomponent = GetComponent<Script2>();
	}
	void Update()
	{
		//PrzezFindObject();
		//PrzezMessage();
		PrzezSingleton();
	}
	public void PrzezPublic()
	{
		Debug.Log(publicZmienna.znaki);
	}
	public void PrzezGetComponent()
	{
		Debug.Log(przezKomponent.znaki);
	}
	public void PrzezSingleton()
	{
		Debug.Log(Script3.access.znaki);
	}
	public void PrzezFindObject()
	{
		Script4 rzecz = (Script4)FindObjectOfType(typeof(Script4));
		Debug.Log(rzecz.znaki);
	}
	public void PrzezMessage()
	{
		gameObject.BroadcastMessage("msg");
	}
}
